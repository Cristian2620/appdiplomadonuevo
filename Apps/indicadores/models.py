from django.db import models
from django.utils.timezone import now

#Clase de Proyecto para la base de datos

class Medicion(models.Model):
    hum_ambiente = models.IntegerField()
    tem_ambiente = models.IntegerField()
    hum_suelo    = models.IntegerField(null=True)
    fecha_creacion = models.DateTimeField(default=now())

    
