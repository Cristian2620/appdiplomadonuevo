from django.urls import path
from .views import *

app_name = 'authenticate'

urlpatterns = [
    path('', home, name="login"),
    path('logout/', user_logout, name="logout")
]