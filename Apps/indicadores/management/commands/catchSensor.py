from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now
from Apps.indicadores.models import Medicion
import requests
import json
from django.conf import settings
import os

class Command(BaseCommand):
    help = 'Consulta las mediciones de los sensores en momento de tiempo real.'

    def handle(self, *args, **options):
        try:
            ngrok = os.getenv('NGROK')
            if ngrok is None:
                ngrok = settings.NGROK_LOCAL
            
            response = requests.get(ngrok)
            sendata = response.json()
            newRecord = Medicion(hum_ambiente=sendata['Humedad_Ambiente'], tem_ambiente=sendata['Temperatura_Ambiente'], hum_suelo=sendata['Humedad_Suelo'])
            newRecord.save()
            self.stdout.write(self.style.SUCCESS('Registro insertado: %s \n Hora registro: %s' % (sendata, now())))
        except Exception as e:
            raise CommandError('Fallo en el registo, error: %s \n Hora de fallo: %s' % (str(e), now()))