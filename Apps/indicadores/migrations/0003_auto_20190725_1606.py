# Generated by Django 2.2.3 on 2019-07-25 21:06

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('indicadores', '0002_medicion_hum_suelo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medicion',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 25, 21, 6, 51, 524473, tzinfo=utc)),
        ),
    ]
