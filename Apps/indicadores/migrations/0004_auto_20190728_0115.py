# Generated by Django 2.2.3 on 2019-07-28 01:15

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('indicadores', '0003_auto_20190725_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medicion',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 28, 1, 15, 41, 769375)),
        ),
    ]
