from django.shortcuts import render, redirect
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib.auth.decorators import login_required
# Create your views here.

def home(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('password')
        nextPage = request.POST.get('next')
        if form.is_valid():
            user = authenticate(username=username, password=password)
            if user.is_active:
                login(request,user)
                if nextPage:
                    return redirect(nextPage)
                else:
                    return redirect("rep_dia")
    else:
        form = LoginForm
    return render(request,'login.html',  {'form':form})

@login_required
def user_logout(request):
    logout(request)
    return redirect("authenticate:login")