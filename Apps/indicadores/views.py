from django.shortcuts import render
from django.shortcuts import render
from .models import Medicion
import requests
import json
from django.http import HttpResponse,HttpResponseRedirect
from django.utils.timezone import now
from django.contrib.auth.decorators import login_required
from django.conf import settings
import os
from django.utils.timezone import datetime as DateTime
import datetime
import json


# Create your views here.

@login_required
def rep_dia(request):
    fecha = ""
    if (request.method == 'POST'):
        fecha = request.POST['fecha_consulta']
        start_date = fecha + " 00:00:00.000000"
        end_date = fecha + " 23:59:59.999999"
        val_mediciones = Medicion.objects.filter(fecha_creacion__range=(start_date,end_date))
        print (start_date,end_date)
        contexto = {
            'val_mediciones' : val_mediciones,
            'fecha': fecha,
        }
        
        return render(request, 'reporte_diario.html', contexto)
    else:
        return render(request,'reporte_diario.html')

@login_required
def logicRest(request):
    print(now())
    ngrok = os.getenv('NGROK')
    if ngrok is None:
        ngrok = settings.NGROK_LOCAL
    response = requests.get(ngrok)
    sendata = response.json()
    newRecord = Medicion(hum_ambiente=sendata['Humedad_Ambiente'], tem_ambiente=sendata['Temperatura_Ambiente'], hum_suelo=sendata['Humedad_Suelo'])
    newRecord.save()
    return HttpResponse()

@login_required
def med_diaria(request):
    today = DateTime.today()
    start_date = datetime.datetime(today.year, today.month, today.day)
    end_date = datetime.datetime(today.year, today.month, today.day, 23,59,59)
    val_mediciones = Medicion.objects.filter(fecha_creacion__range=(start_date,end_date))
    humedades = []
    temperaturas = []
    ambientes = []
    fechas = []
    for medicion in val_mediciones:
        humedades.append(medicion.hum_ambiente)
        temperaturas.append(medicion.tem_ambiente)
        ambientes.append(medicion.hum_suelo)
        fechas.append(medicion.fecha_creacion)
    
    horas = []
    for hora in fechas:
        horas.append(hora.strftime("%H:%M:%S"))

    horas = json.dumps(horas)

    context = {
        'humedades': humedades,
        'temperaturas': temperaturas,
        'ambientes': ambientes,
        'horas': horas
    }
    return render(request,'medi_diaria.html', context)

